FROM ubuntu:18.04

RUN apt-get update && apt-get --quiet install --yes cmake gcc g++ git sudo curl openssl libssl-dev libgles2-mesa-dev valgrind unzip ninja-build libglfw3-dev gcovr doxygen graphviz libboost-dev && rm -rf /var/lib/apt/lists/*
